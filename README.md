# Plugins/themes for updated bbPress/BuddyPress.org sites.

# Important
* This is a private repo.
* Commits or Pushes to master will auto-deploy to http://dev-bporg.pantheonsite.io/ (hosted on Pantheon).
 * Paul owns the Pantheon.
* Repo includes third-party theme, "", from arraythemes.com. **DO NOT REDISTRIBUTE**.

# Less important
* Plugin `wp-redis` only included for Pantheon, not intended for .org release.

# Local installation
* Create a `wp-config-local.php` file, set usual DB constants (etc) and `WP_DEBUG`. Point it at Salty WordPress or your favourite web server.
