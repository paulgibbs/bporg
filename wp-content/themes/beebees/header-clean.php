<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Paperback
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header id="masthead" class="site-header" role="banner">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'beebees' ); ?></a>

	<div class="top-navigation">
		<div class="container">

			<nav id="secondary-navigation" class="main-navigation secondary-navigation" role="navigation">
				<?php if ( has_nav_menu( 'secondary' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'secondary'
					) );
				} ?>
			</nav><!-- .secondary-navigation -->

			<div class="top-navigation-right">
				<?php if ( has_nav_menu( 'social' ) ) { ?>
					<nav class="social-navigation" role="navigation">
						<?php wp_nav_menu( array(
							'theme_location' => 'social',
							'depth'          => 1,
							'fallback_cb'    => false
						) );?>
					</nav><!-- .social-navigation -->
				<?php } ?>

				<div class="overlay-toggle drawer-toggle drawer-open-toggle" tab-index="0">
					<span class="toggle-visible">
						<i class="fa fa-search"></i>
						<?php esc_html_e( 'Explore', 'paperback' ); ?>
					</span>
					<span>
						<i class="fa fa-times"></i>
						<?php esc_html_e( 'Close', 'paperback' ); ?>
					</span>
				</div><!-- .overlay-toggle-->

				<div class="overlay-toggle drawer-toggle drawer-menu-toggle" tab-index="0">
					<span class="toggle-visible">
						<i class="fa fa-bars"></i>
						<?php esc_html_e( 'Menu', 'paperback' ); ?>
					</span>
					<span>
						<i class="fa fa-times"></i>
						<?php esc_html_e( 'Close', 'paperback' ); ?>
					</span>
				</div><!-- .overlay-toggle-->
			</div><!-- .top-navigation-right -->
		</div><!-- .container -->
	</div><!-- .top-navigation -->

	<div class="drawer-wrap">
		<?php
			// Get the explore drawer (template-parts/content-drawer.php)
			get_template_part( 'template-parts/content-drawer' );

			// Get the explore drawer (template-parts/content-menu-drawer.php)
			get_template_part( 'template-parts/content-menu-drawer' );
		?>
	</div><!-- .drawer-wrap -->

	<div class="site-identity clear">
		<div class="container">
			<!-- Site title and logo -->
			<?php
				paperback_title_logo();

				$mega_menu = get_theme_mod( 'paperback_category_menu', 'disabled' );
			?>

			<!-- Main navigation -->
			<nav id="site-navigation" class="main-navigation <?php echo esc_attr( $mega_menu ); ?>" role="navigation">
				<?php wp_nav_menu( array(
					'theme_location' => 'primary'
				) );?>
			</nav><!-- .main-navigation -->

		</div><!-- .container -->
	</div><!-- .site-identity-->

</header><!-- .site-header -->

<div id="page" class="hfeed site container">
	<div id="content" class="site-content">
