<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 *
 * @package Beebees
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'beebees-front-page' );
			endwhile;
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif;

		// Get each of our panels and show the post data
		$panels = array( '1', '2', '3' );
		$titles = array();

		global $beebeescounter; // Used in content-beebees-front-page.php

		if ( 0 !== beebees_panel_count() ) : //If we have pages to show...

			$beebeescounter = 1;

			foreach ( $panels as $panel ) :

				if ( get_theme_mod( 'beebees_panel' . $panel ) ) :

					$post = get_post( get_theme_mod( 'beebees_panel' . $panel ) );

					setup_postdata( $post );

					set_query_var( 'beebees_panel', $panel );

					get_template_part( 'template-parts/content', 'beebees-front-page' );

					$titles[] = get_the_title(); //Put page titles in an array for use in navigation

					wp_reset_postdata();

				endif; // if ( get_theme_mod( 'beebees_panel' . $panel ) )

				$beebeescounter++;

			endforeach; // foreach ( $panels as $panel )


		endif; // if ( 0 !== beebees_panel_count() )

		?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
