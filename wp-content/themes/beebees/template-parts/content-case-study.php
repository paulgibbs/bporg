<?php
/*
 *	Content of Case Studies Archive
 *
 * @package Beebees
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>

	<div class="case-study-text">
		<h2 class="entry-case-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<span class="case-author-avatar"><?php echo get_avatar( get_the_author_meta('email'), '50' ); ?></span>
		<P class="case-study-author"><?php esc_html_e( 'By ', 'beebees' ); ?><?php the_author(); ?> <?php beebees_edit_link(); ?></p>
	</div>

	<!-- Grab the featured image -->
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="case-study-featured-image"><?php the_post_thumbnail( 'paperback-hero' ); ?></div>
	<?php } ?>

</article><!-- #post-## -->
