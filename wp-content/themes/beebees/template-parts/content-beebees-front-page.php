<?php
/**
 * Displays content for front page
 *
 * @package Beebees
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'beebees-panel ' ); ?> >

	<div class="panel-content">
		<div class="beebees-wrap">

			<header class="entry-header">
				<?php if ( is_front_page() ) {
					the_title( '<h2 class="entry-title">', '</h2>' );
				} else {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} ?>

				<?php beebees_edit_link( get_the_ID() ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					the_content( sprintf(
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'beebees' ),
						get_the_title()
					) );
				?>
			</div><!-- .entry-content -->

		</div><!-- .beebees-wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
