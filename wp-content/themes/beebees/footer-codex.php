<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Paperback
 */
?>

	</div><!-- #content -->
</div><!-- #page -->

<?php if ( is_singular( 'page' ) ) { ?>

	<div class="footer-meta-wrap">

		<div class="footer-meta">

			<div class="col-half">

				<h3 class="title">Article Contributors</h3>

				<?php

				global $codex_contributors, $post;

				if ( count( $codex_contributors ) ) {

					if ( count( $codex_contributors ) ) {
						echo '<div class="contributors">';
						$codex_contributors = array_slice( $codex_contributors, 0, 3, true );
						foreach( (array)$codex_contributors as $contributor_id => $count ) {
							$userdata = get_userdata( $contributor_id );
							echo '<div class="contributor">';
								echo '<a href="#"><div class="contributor-avatar float-left">';
									echo get_avatar( $contributor_id, 48 );
									echo '<div class="revision-count">' . esc_html( $count ) . '</div>';
									echo '<div class="contributor-name"><span>' . esc_html( $userdata->display_name ) . '</span></div>';
								echo '</div></a>';
							echo '</div>';
						}
						echo '</div>';
					}
				}
				?>

				<p class="date">Updated <strong><?php echo human_time_diff( get_the_modified_time( 'U', get_queried_object_id() ) ); ?></strong> ago / Published <strong><?php echo human_time_diff( get_the_time( 'U', get_queried_object_id() ) ); ?></strong> ago</p>

			</div>

			<div class="col-half">

			<h3 class="title">Want to help?</h3>

			<p>The BuddyPress Codex is volunteer-powered, which means you can contribute too! If you're interested in updating existing articles or creating entirely new ones, please read our <a href="https://codex.buddypress.org/participate-and-contribute/codex-standards-guidelines/">Codex Standards & Guidelines</a>.</p>

			</div>

		</div>

	</div>

<?php } ?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="footer-bottom">
			<?php if ( has_nav_menu( 'footer' ) ) { ?>
				<nav class="footer-navigation" role="navigation">
					<?php wp_nav_menu( array(
						'theme_location' => 'footer',
						'depth'          => 1,
						'fallback_cb'    => false
					) );?>
				</nav><!-- .footer-navigation -->
			<?php } ?>
		</div><!-- .footer-bottom -->
	</div><!-- .container -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
