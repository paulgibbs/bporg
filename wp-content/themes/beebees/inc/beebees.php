<?php
/**
 * Beebees Theme Extras
 *
 * @package Beebees
 */

// Add custom classes to the array of body classes.
function beebees_body_classes( $classes ) {

	if ( is_customize_preview() ) {
		$classes[] = 'beebees-customizer';
	}

	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'beebees-front-page';
	}

	return $classes;
}
add_filter( 'body_class', 'beebees_body_classes' );

// Count our number of active panels
function beebees_panel_count() {
	$panels = array( '1', '2', '3', '4', '5' );
	$panel_count = 0;
	foreach ( $panels as $panel ) :
		if ( get_theme_mod( 'beebees_panel' . $panel ) ) :
			$panel_count++;
		endif;
	endforeach;
	return $panel_count;
}

// Template Tags
function beebees_edit_link() {
	edit_post_link(
		sprintf(
			__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'beebees' ),
			get_the_title()
		),
		'<span class="edit-link">',
		'</span>'
	);
}
