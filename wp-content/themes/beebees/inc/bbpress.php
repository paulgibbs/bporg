<?php
/**
 * bbPress add-ons
 *
 * @package Beebees
 * @author johnjamesjacoby
 */

//  Allow styles for the Lead Topic
add_filter( 'bbp_show_lead_topic', '__return_true' );

// Forums Sidebar - bb_base_*
function bb_base_topic_search_form() {
	?>
	<form role="search" method="get" id="searchform" action="">
		<div class="widget">
			<h2 class="widget-title"><?php _e( 'Forum Search', 'bbporg'); ?></h2>
			<label class="screen-reader-text hidden" for="ts"><?php _e( 'Search for:', 'bbporg' ); ?></label>
			<input type="text" value="<?php echo bb_base_topic_search_query(); ?>" name="ts" id="ts" />
			<input class="button" type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'bbporg' ); ?>" />
		</div>
	</form>
<?php
}

function bb_base_reply_search_form() {
	?>
	<form role="search" method="get" id="searchform" action="">
		<div class="widget">
			<h2 class="widget-title"><?php _e( 'Reply Search', 'bbporg'); ?></h2>
			<label class="screen-reader-text hidden" for="rs"><?php _e( 'Search for:', 'bbporg' ); ?></label>
			<input type="text" value="<?php echo bb_base_reply_search_query(); ?>" name="rs" id="rs" />
			<input class="button" type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'bbporg' ); ?>" />
		</div>
	</form>
<?php
}

function bb_base_topic_search_query( $escaped = true ) {

	if ( empty( $_GET['ts'] ) ) {
		return false;
	}

	$query = apply_filters( 'bb_base_topic_search_query', $_GET['ts'] );
	if ( true === $escaped ) {
		$query = stripslashes( esc_attr( $query ) );
	}

	return $query;
}

function bb_base_reply_search_query( $escaped = true ) {

	if ( empty( $_GET['rs'] ) ) {
		return false;
	}

	$query = apply_filters( 'bb_base_reply_search_query', $_GET['rs'] );
	if ( true === $escaped ) {
		$query = stripslashes( esc_attr( $query ) );
	}

	return $query;
}

/**
 * Output first page of support topics
 *
 * @author johnjamesjacoby
 * @uses bb_base_get_homepage_topics()
 * @param mixed $args
 * @return void
 */
function bb_base_support_topics() {
	echo bb_base_get_support_topics();
}

/**
 * Get first page of support topics output and stash it for an our
 *
 * @author johnjamesjacoby
 * @param mixed $args
 * @return HTML
 */
function bb_base_get_support_topics() {

	// Transient settings
	$expiration    = MINUTE_IN_SECONDS * 5;
	$transient_key = 'bb_base_support_topics';
	$output        = get_transient( $transient_key );

	// No transient found, so query for topics again
	if ( false === $output ) {

		// Look for topics
		$output = bbp_buffer_template_part( 'content', 'archive-topic', false );

		// Set the transient
		set_transient( $transient_key, $output, $expiration );
	}

	// Return the output
	return $output;
}

/**
 * Purge first page of topics cache when bbPress's post cache is cleaned.
 *
 * This allows the support topics fragment cache to be updated when new topics
 * and replies are created in the support forums.
 *
 * @author johnjamesjacoby
 * @return void
 */
function bb_base_purge_support_topics() {
	delete_transient( 'bb_base_support_topics' );
}
add_action( 'bbp_clean_post_cache', 'bb_base_purge_support_topics' );


function bb_base_single_topic_description() {

	// Validate topic_id
	$topic_id = bbp_get_topic_id();

	// Unhook the 'view all' query var adder
	remove_filter( 'bbp_get_topic_permalink', 'bbp_add_view_all' );

	// Build the topic description
	$voice_count = bbp_get_topic_voice_count   ( $topic_id, true );
	$reply_count = bbp_get_topic_replies_link  ( $topic_id );
	$time_since  = bbp_get_topic_freshness_link( $topic_id );

	// Singular/Plural
	$voice_count = sprintf( _n( '%s participant', '%s participants', $voice_count, 'beebees' ), bbp_number_format( $voice_count ) );
	$last_reply  = bbp_get_topic_last_active_id( $topic_id );

	?>

	<li class="topic-forum"><?php
		/* translators: %s: forum title */
		printf( __( 'In: %s', 'beebees' ),
			sprintf( '<a href="%s">%s</a>',
				esc_url( bbp_get_forum_permalink( bbp_get_topic_forum_id() ) ),
				bbp_get_topic_forum_title()
			)
		);
	?></li>
	<?php if ( ! empty( $reply_count ) ) : ?>
		<li class="reply-count"><?php echo $reply_count; ?></li>
	<?php endif; ?>
	<?php if ( ! empty( $voice_count ) ) : ?>
		<li class="voice-count"><?php echo $voice_count; ?></li>
	<?php endif; ?>
	<?php if ( ! empty( $last_reply  ) ) : ?>
		<li class="topic-freshness-author"><?php
			/* translators: %s: reply author link */
			printf( __( 'Last reply from: %s', 'beebees' ),
				bbp_get_author_link( array( 'type' => 'name', 'post_id' => $last_reply, 'size' => '15' ) )
			);
		?></li>
	<?php endif; ?>
	<?php if ( ! empty( $time_since  ) ) : ?>
		<li class="topic-freshness-time"><?php
			/* translators: %s: date/time link to the latest post */
			printf( __( 'Last activity: %s', 'beebees' ), $time_since );
		?></li>
	<?php endif; ?>
	<?php if ( is_user_logged_in() ) : ?>
		<?php $_topic_id = bbp_is_reply_edit() ? bbp_get_reply_topic_id() : $topic_id; ?>
		<li class="topic-subscribe"><?php bbp_topic_subscription_link( array( 'before' => '', 'topic_id' => $_topic_id ) ); ?></li>
		<li class="topic-favorite"><?php bbp_topic_favorite_link( array( 'topic_id' => $_topic_id ) ); ?></li>
	<?php endif; ?>
	<?php if ( function_exists( 'WordPressdotorg\Forums\Topic_Resolution\get_topic_resolution_form' ) ) : ?>
		<?php if ( WordPressdotorg\Forums\Topic_Resolution\Plugin::get_instance()->is_enabled_on_forum() && ( bbp_is_single_topic() || bbp_is_topic_edit() ) ) : ?>
			<li class="topic-resolved"><?php WordPressdotorg\Forums\Topic_Resolution\get_topic_resolution_form( $topic_id ); ?></li>
		<?php endif; ?>
	<?php endif;
}

function bb_base_single_forum_description() {

	// Validate forum_id
	$forum_id = bbp_get_forum_id();

	// Unhook the 'view all' query var adder
	remove_filter( 'bbp_get_forum_permalink', 'bbp_add_view_all' );

	// Get some forum data
	$topic_count = bbp_get_forum_topic_count( $forum_id, true, true );
	$reply_count = bbp_get_forum_reply_count( $forum_id, true, true );
	$last_active = bbp_get_forum_last_active_id( $forum_id );

	// Has replies
	if ( !empty( $reply_count ) ) {
		$reply_text = sprintf( _n( '%s reply', '%s replies', $reply_count, 'beebees' ), bbp_number_format( $reply_count ) );
	}

	// Forum has active data
	if ( !empty( $last_active ) ) {
		$topic_text      = bbp_get_forum_topics_link( $forum_id );
		$time_since      = bbp_get_forum_freshness_link( $forum_id );

	// Forum has no last active data
	} else {
		$topic_text      = sprintf( _n( '%s topic', '%s topics', $topic_count, 'beebees' ), bbp_number_format( $topic_count ) );
	}
	?>

	<?php if ( bbp_get_forum_parent_id() ) : ?>
		<li class="topic-parent">In: <a href="<?php bbp_forum_permalink( bbp_get_forum_parent_id() ); ?>"><?php bbp_forum_title( bbp_get_forum_parent_id() ); ?></a>
		</li>
	<?php endif; ?>

	<?php if ( ! empty( $topic_count ) ) : ?>
		<li class="topic-count"><?php echo $topic_text; ?></li>
	<?php endif; ?>

	<?php if ( ! empty( $reply_count ) ) : ?>
		<li class="reply-count"><?php echo $reply_text; ?></li>
	<?php endif; ?>

	<?php if ( ! empty( $last_active  ) ) : ?>
		<li class="forum-freshness-author"><?php printf( __( 'Last post by: %s', 'bbporg' ), bbp_get_author_link( array( 'type' => 'name', 'post_id' => $last_active ) ) ); ?></li>
	<?php endif; ?>

	<?php if ( ! empty( $time_since  ) ) : ?>
		<li class="forum-freshness-time"><?php printf( __( 'Last activity: %s', 'bbporg' ), $time_since ); ?></li>
	<?php endif; ?>

	<?php if ( is_user_logged_in() ) : ?>
		<li class="forum-subscribe"><?php bbp_forum_subscription_link( array( 'forum_id' => $forum_id ) ); ?></li>
	<?php endif;
}

/**
 * Change "Stick (to front)" link text to "Stick (to all forums)".
 */
function beebees_change_super_sticky_text( $links ) {
	if ( isset( $links['stick'] ) ) {
		$links['stick'] = bbp_get_topic_stick_link( array( 'super_text' => __( '(to all forums)', 'beebees' ) ) );
	}

	return $links;
}
add_filter( 'bbp_topic_admin_links', 'beebees_change_super_sticky_text' );
