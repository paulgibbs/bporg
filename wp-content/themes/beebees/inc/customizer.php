<?php
/**
 * Beebees: Customizer
 *
 * @package Beebees
 */

function beebees_customize_register( $wp_customize ) {

	// Panel - Beebees Theme Options.
	$wp_customize->add_panel( 'beebees_theme_options', array(
		'title'    => __( 'Beebees Theme Options', 'beebees' ),
		'priority' => 11,
	) );

	// Section - Site Stylesheets
	$wp_customize->add_section( 'beebees_custom_options', array(
		'title'       => __( 'Custom Beebees Stylesheets', 'beebees' ),
		'capability'  => 'edit_theme_options',
		'description' => __( 'Select site to enqueue custom stylesheet.',  'beebees'  ),
		'panel'       => 'beebees_theme_options',
		'priority'    => 10,
	) );

	$wp_customize->add_setting( 'beebees_stylesheets', array(
		'default'           => 'base',
		'capability'        => 'manage_options',
		'transport'         => 'postMessage',
		'type'              => 'theme_mod',
		'sanitize_callback' => 'beebees_sanitize_custom_stylesheets',
	) );

	$wp_customize->add_control( 'beebees_stylesheets', array(
		'type'        => 'select',
		'section'     => 'beebees_custom_options',
		'settings'    => 'beebees_stylesheets',
		'label'       => __( 'Beebees Site', 'beebees' ),
		'description' => __( 'A specific stylesheet is associated with each site.', 'beebees'  ),
		'choices'     => array(
			'base'         => __( 'Default none', 'beebees' ),
			'bp-org.css'   => __( 'BuddyPress.org', 'beebees' ),
			'bb-org.css'   => __( 'bbPress.org', 'beebees' ),
			'bp-codex.css' => __( 'codex.BuddyPress.org', 'beebees' ),
			'bb-codex.css' => __( 'codex.bbPress.org', 'beebees' ),
			'bp-dev.css'   => __( 'developer.BuddyPress.org', 'beebees' ),
			'bb-dev.css'   => __( 'developer.bbPress.org', 'beebees' ),
		),
	) );

	// Front Page Panels
	$wp_customize->add_section( 'beebees_panel1', array(
		'title'           => esc_html__( 'Homepage Panel 2', 'beebees' ),
		'active_callback' => 'is_front_page',
		'panel'           => 'beebees_theme_options',
		'description'     => esc_html__( 'Select a published page to show in the front page.', 'beebees' ),
		'priority'    => 20,
	) );

	$wp_customize->add_setting( 'beebees_panel1', array(
		'default'           => false,
		'sanitize_callback' => 'beebees_sanitize_numeric_value',
	) );

	$wp_customize->add_control( 'beebees_panel1', array(
		'label'   => esc_html__( '2nd Panel Content', 'beebees' ),
		'section' => 'beebees_panel1',
		'type'    => 'dropdown-pages',
	) );

	$wp_customize->add_section( 'beebees_panel2', array(
		'title'           => esc_html__( 'Homepage Panel 3', 'beebees' ),
		'active_callback' => 'is_front_page',
		'panel'           => 'beebees_theme_options',
		'description'     => esc_html__( 'Select a published page to show in the front page.', 'beebees' ),
		'priority'        => 30,
	) );

	$wp_customize->add_setting( 'beebees_panel2', array(
		'default'           => false,
		'sanitize_callback' => 'beebees_sanitize_numeric_value',
	) );

	$wp_customize->add_control( 'beebees_panel2', array(
		'label'   => esc_html__( '3rd Panel Content', 'beebees' ),
		'section' => 'beebees_panel2',
		'type'    => 'dropdown-pages',
	) );

	$wp_customize->add_section( 'beebees_panel3', array(
		'title'           => esc_html__( 'Homepage Panel 4', 'beebees' ),
		'active_callback' => 'is_front_page',
		'panel'           => 'beebees_theme_options',
		'description'     => esc_html__( 'Select a published page to show in the front page.', 'beebees' ),
		'priority'        => 40,
	) );

	$wp_customize->add_setting( 'beebees_panel3', array(
		'default'           => false,
		'sanitize_callback' => 'beebees_sanitize_numeric_value',
	) );

	$wp_customize->add_control( 'beebees_panel3', array(
		'label'   => esc_html__( '4th Panel Content', 'beebees' ),
		'section' => 'beebees_panel3',
		'type'    => 'dropdown-pages',
	) );

	$wp_customize->add_section( 'beebees_panel4', array(
		'title'           => esc_html__( 'Homepage Panel 5', 'beebees' ),
		'active_callback' => 'is_front_page',
		'panel'           => 'beebees_theme_options',
		'description'     => esc_html__( 'Select a published page to show in the front page.', 'beebees' ),
		'priority'        => 50,
	) );

	$wp_customize->add_setting( 'beebees_panel4', array(
		'default'           => false,
		'sanitize_callback' => 'beebees_sanitize_numeric_value',
	) );

	$wp_customize->add_control( 'beebees_panel4', array(
		'label'   => esc_html__( '5th Panel Content', 'beebees' ),
		'section' => 'beebees_panel4',
		'type'    => 'dropdown-pages',
	) );

	$wp_customize->add_section( 'beebees_panel5', array(
		'title'           => esc_html__( 'Homepage Panel 6', 'beebees' ),
		'active_callback' => 'is_front_page',
		'panel'           => 'beebees_theme_options',
		'description'     => esc_html__( 'Select a published page to show in the front page.', 'beebees' ),
		'priority'        => 60,
	) );

	$wp_customize->add_setting( 'beebees_panel5', array(
		'default'           => false,
		'sanitize_callback' => 'beebees_sanitize_numeric_value',
	) );

	$wp_customize->add_control( 'beebees_panel5', array(
		'label'   => esc_html__( '6th Panel Content', 'beebees' ),
		'section' => 'beebees_panel5',
		'type'    => 'dropdown-pages',
	) );
}
add_action( 'customize_register', 'beebees_customize_register' );

// Sanitize color scheme
function beebees_sanitize_custom_stylesheets( $input ) {
	$valid = array( 'base', 'bp-org.css', 'bb-org.css', 'bp-codex.css', 'bb-codex.css', 'bp-dev.css', 'bb-dev.css' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}
	return 'base';
}

// Sanitize callbacks
function beebees_sanitize_numeric_value( $input ) {
	if ( is_numeric( $input ) ) {
		return intval( $input );
	} else {
		return 0;
	}
}

function beebees_sanitize_boolean( $input ) {
	if ( is_bool( $input ) ) {
		return $input;
	} else {
		return true;
	}
}

// Bind JS handlers to make the Customizer preview reload changes
function beebees_customize_preview_js() {

	wp_enqueue_script( 'beebees_customizer', get_stylesheet_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '1.0', true );
}
add_action( 'customize_preview_init', 'beebees_customize_preview_js' );

function beebees_panels_js() {

	wp_enqueue_script( 'beebees-customizer-panels', get_stylesheet_directory_uri() . '/js/customizer-panels.js', array(), '20180118', true );
}
add_action( 'customize_controls_enqueue_scripts', 'beebees_panels_js' );
