<?php
/**
 * Template Name: Full Width
 *
 * The Template for displaying a Case Study.
 *
 * @package Beebees
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();

				// Move Jetpack share links below author box
				if ( function_exists( 'sharing_display' ) && ! function_exists( 'dsq_comment' ) ) {
					remove_filter( 'the_content', 'sharing_display', 19 );
					remove_filter( 'the_excerpt', 'sharing_display', 19 );
				}

				// Post content template
				get_template_part( 'template-parts/content' );

				comments_template();

			endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
