<?php

/**
 * Single Topic Part
 *
 * @package bbPress
 * @subpackage Beebees
 */

?>

<?php do_action( 'bbp_template_before_lead_topic' ); ?>

<ul id="bbp-topic-<?php bbp_topic_id(); ?>-lead" class="bbp-lead-topic">

	<li class="bbp-body">

		<div id="post-<?php bbp_topic_id(); ?>" <?php bbp_topic_class(); ?>>

			<?php if ( bbp_is_topic_sticky() ) : ?>
				<div class="topic-indicator">
					<span class="dashicons dashicons-admin-post" aria-hidden="true"></span><?php esc_html_e( 'Sticky', 'beebees' ); ?>
				</div>
			<?php elseif ( bbp_is_topic_closed() ) : ?>
				<div class="topic-indicator">
					<span class="dashicons dashicons-lock" aria-hidden="true"></span><?php esc_attr_e( 'Closed', 'beebees' ); ?>
				</div>
			<?php endif; ?>

			<div class="bbp-topic-author">

				<?php do_action( 'bbp_theme_before_topic_author_details' ); ?>

				<?php bbp_topic_author_link( array( 'sep' => '', 'show_role' => false, 'size' => 100 ) ); ?>

				<?php bbp_user_nicename( bbp_get_topic_author_id(), array( 'before' => '<span class="bbp-user-nicename">(@', 'after' => ')</span><br />' ) ); ?>

				<div class="bbp-author-role">
					<?php echo bbp_get_user_display_role( bbp_get_topic_author_id( bbp_get_topic_id() ) ); ?>
				</div>

				<p class="bbp-topic-post-date"><a href="<?php bbp_topic_permalink(); ?>" title="#<?php bbp_topic_id(); ?>" class="bbp-topic-permalink"><?php bbp_topic_post_date( bbp_get_topic_id(), true ); ?></a></p>

				<?php if ( is_super_admin() ) : ?>

					<?php do_action( 'bbp_theme_before_topic_author_admin_details' ); ?>

					<div class="bbp-topic-ip"><?php bbp_author_ip( bbp_get_topic_id() ); ?></div>

					<?php do_action( 'bbp_theme_after_topic_author_admin_details' ); ?>

				<?php endif; ?>

				<?php do_action( 'bbp_theme_after_topic_author_details' ); ?>

			</div><!-- .bbp-topic-author -->

			<div class="bbp-topic-content">

				<?php do_action( 'bbp_theme_before_topic_content' ); ?>

				<?php bbp_topic_content(); ?>

				<?php do_action( 'bbp_theme_after_topic_content' ); ?>

			</div><!-- .bbp-topic-content -->

			<?php bbp_topic_admin_links(); ?>

		</div><!-- #post-<?php bbp_topic_id(); ?> -->

	</li><!-- .bbp-body -->

</ul><!-- #topic-<?php bbp_topic_id(); ?>-replies -->

<?php do_action( 'bbp_template_after_lead_topic' ); ?>
