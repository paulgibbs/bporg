<?php
/**
 * Forums Loop - Single Forum
 *
 * @package bbPress
 * @subpackage Beebees
 */

?>

<ul id="bbp-forum-<?php bbp_forum_id(); ?>" <?php bbp_forum_class(); ?>>

	<li class="bbp-forum-info">
		<a class="bbp-forum-title" href="<?php bbp_forum_permalink(); ?>"><?php bbp_forum_title(); ?></a>
	</li>
	<li class="bbp-forum-topic-count"><?php bbp_forum_post_count(); ?></li>

</ul><!-- #bbp-forum-<?php bbp_forum_id(); ?> -->
