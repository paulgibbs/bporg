<?php
/**
 * Template notice for no search input.
 *
 * @package  Beebees
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'Please enter some search terms above', 'bbpress' ); ?></p>
</div>
