<?php
/**
 * Template Name: Codex Articles
 *
 * @package Beebees
 */

get_header( 'clean' ); ?>

<?php get_sidebar( 'codex' ); ?>

<div id="primary" class="content-area content-has-left-sidebar">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post();

			// Page content template
			get_template_part( 'template-parts/content-page-codex' );

		endwhile; ?>

	</main><!-- #main -->

</div><!-- #primary -->

<hr class="hidden" />

<?php
	global $post;

	$args         = array( 'order' => 'ASC', );
	$revisions    = wp_get_post_revisions( get_queried_object_id(), $args );
	$post_authors = array( $post->post_author => 1 );
	foreach( (array)$revisions as $revision ) {
		$post_authors[$revision->post_author] += 1;
	}
	asort( $post_authors, SORT_NUMERIC );

	global $codex_contributors;
	$codex_contributors = array_reverse( $post_authors, true );
?>

<?php get_footer( 'codex' ); ?>
