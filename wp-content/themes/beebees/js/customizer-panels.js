/**
 * Theme Customizer enhancements - detect when user has opened panels
 */

( function( $ ) {

	wp.customize.bind( 'ready', function() {

		wp.customize.section( 'beebees_panel1' ).expanded.bind( function( isExpanding ) {

			wp.customize.previewer.send( 'section-highlight', { section: 'beebees-panel1', expanded: isExpanding } );
		} );

		wp.customize.section( 'beebees_panel2' ).expanded.bind( function( isExpanding ) {
			wp.customize.previewer.send( 'section-highlight', { section: 'beebees-panel2', expanded: isExpanding } );
		} );

		wp.customize.section( 'beebees_panel3' ).expanded.bind( function( isExpanding ) {
			wp.customize.previewer.send( 'section-highlight', { section: 'beebees-panel3', expanded: isExpanding } );
		} );

		wp.customize.section( 'beebees_panel4' ).expanded.bind( function( isExpanding ) {
			wp.customize.previewer.send( 'section-highlight', { section: 'beebees-panel4', expanded: isExpanding } );
		} );

		wp.customize.section( 'beebees_panel5' ).expanded.bind( function( isExpanding ) {
			wp.customize.previewer.send( 'section-highlight', { section: 'beebees-panel5', expanded: isExpanding } );
		} );

	} );

} )( jQuery );
