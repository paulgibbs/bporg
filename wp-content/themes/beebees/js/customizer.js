/**
 * File customizer.js.
 *
 * Instantly live-update customizer settings in the preview for improved user experience.
 */

(function( $ ) {

	// Collect information from panel-customizer.js about which panels are opening
	wp.customize.bind( 'preview-ready', function() {
		wp.customize.preview.bind( 'section-highlight', function( data ) {

			if ( $( '.' + data.section ).length ) {

				if ( true === data.expanded ) {

					$( 'body' ).animate( {
						scrollTop: $( '.' + data.section ).offset().top - $( '.site-header' ).outerHeight(), 
					}, 600 );

					$( '.' + data.section ).addClass( 'beebees-highlight' );

				} else {
					$( 'body' ).animate( {
						scrollTop: 0,
					}, 600 );
					$( '.' + data.section ).removeClass( 'beebees-highlight' );
				}
			}
		} );
	} );

	// Color and Layout schemes to follow.

} )( jQuery );
