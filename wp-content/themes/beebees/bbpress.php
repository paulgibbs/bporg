<?php
/**
 * Displays content for forums.
 *
 * @package Beebees
 */

get_header( 'clean' ); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post();

			// Page content template
			get_template_part( 'template-parts/content-page' );

		endwhile; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
	get_sidebar( 'bbpress' );
	get_footer( 'clean' );
