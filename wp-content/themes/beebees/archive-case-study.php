<?php
/**
 * The template for displaying Case Study Archive.
 *
 * @package Beebees
 */
get_header(); ?>

	<h1 class="entry-title"><?php esc_html_e( 'Case Studies', 'beebees' ); ?></h1>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<div class="archive-case-study">

					<?php
						// Get the post content
						while ( have_posts() ) : the_post();
							// Move Jetpack share links below author box
							if ( function_exists( 'sharing_display' ) && ! function_exists( 'dsq_comment' ) ) {
								remove_filter( 'the_content', 'sharing_display', 19 );
								remove_filter( 'the_excerpt', 'sharing_display', 19 );
							}

							get_template_part( 'template-parts/content-case-study' );

						endwhile;
					?>

				</div><!-- .archive-case-study -->

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
