<?php
// Load parent theme CSS.
add_action( 'wp_enqueue_scripts', function() {
	wp_dequeue_style( 'bbp-default' );
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'paperback-style', get_template_directory_uri() . '/style.css' );

	$beebees_site = get_theme_mod( 'beebees_stylesheets' );
	wp_enqueue_style( 'beebees-stylesheets', get_stylesheet_directory_uri() . '/css/'. $beebees_site );
} );

// "paperback-style" only works at priority 10; these need to be >10.
add_action( 'wp_enqueue_scripts', function() {
	wp_dequeue_style( 'paperback-fonts' );
	wp_dequeue_script( 'responsive-slides' );
	wp_dequeue_script( 'touchSwipe' );
	wp_dequeue_script( 'paperback-js' );

	// Had to change script handle for WP to load from the child theme.
	wp_enqueue_script(
		'beebees-paperback-js',
		get_stylesheet_directory_uri() . '/js/paperback.js',
		array( 'jquery' ),
		'1.0',
		true
	);

	// Improves accessibility of skip link to content.
	wp_enqueue_script(
		'skip-link-focus-fix',
		get_stylesheet_directory_uri() . '/js/skip-link-focus-fix.js',
		'1.0',
		true
	);

	wp_localize_script( 'beebees-paperback-js', 'paperback_js_vars', array(
		'ajaxurl'    => admin_url( 'admin-ajax.php' ),
		'load_fixed' => ( 'enabled' === get_theme_mod( 'paperback_fixed_bar', 'enabled' ) ) ? 'true' : 'false',
	) );
}, 15 );

add_action( 'after_setup_theme', function() {
	// Remove parent theme nag.
	remove_action( 'tgmpa_register', 'paperback_register_required_plugins' );
	remove_action( 'admin_init', 'paperback_redirect_on_activation' );
}, 20 );

add_action( 'admin_menu', function() {
	// Remove parent theme about page.
	remove_submenu_page( 'themes.php', 'paperback-license' );
}, 20 );

/**
 * Adds the project's logo to the Global Header menu.
 *
 * @param string $items The HTML list content for the menu items.
 *
 * @return string
 */
add_filter( 'wp_nav_menu_global-header_items', function( $items ) {
	if ( ! function_exists( '\beebees\settings\is_buddypress_dot_org' ) ) {
		return $items;
	}

	// Get the first menu item.
	if ( ! preg_match( '#<a href="[^"]+">(?P<text>[^<]+)</a></li>#i', $items, $match, PREG_OFFSET_CAPTURE ) ) {
		return $items;
	}

	if ( beebees\settings\is_buddypress_dot_org() ) {
		$image = sprintf(
			'<img src="%1$s" alt="%2$s" title="%2$s">',
			esc_url( get_stylesheet_directory_uri() . '/images/buddypress.svg' ),
			esc_attr__( 'The BuddyPress homepage', 'beebees' )
		);
	} else {
		$image = sprintf(
			'<img src="%1$s" alt="%2$s" title="%2$s">',
			esc_url( get_stylesheet_directory_uri() . '/images/bbpress.svg' ),
			esc_attr__( 'The bbPress homepage', 'beebees' )
		);
	}

	$items = substr_replace( $items, $image, $match['text'][1], strlen( $match['text'][0] ) );

	return $items;
} );

/**
 * Conditionally highlight items in the global header menu.
 *
 * @param array    $classes Current menu classes.
 * @param \WP_Post $item    Current menu item.
 * @param stdClass $args    Menu arguments.
 *
 * @return array $classes
 */
add_filter( 'nav_menu_css_class', function( $classes, $item, $args ) {
	if ( ! function_exists( '\beebees\settings\is_buddypress_dot_org' ) ) {
		return $classes;
	}

	if ( $args->theme_location !== 'secondary' || $args->depth > 0 || in_array( 'current-menu-item', $classes, true ) ) {
		return $classes;
	}

	$current_url  = is_ssl() ? 'https://' : 'http://';
	$current_url .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$current_url  = untrailingslashit( $current_url );
	$forum_slug   = \beebees\settings\is_buddypress_dot_org() ? '/support' : '/forums';

	// Forums.
	if ( $item->post_title === 'Support' && strpos( $current_url, home_url( $forum_slug ) ) !== false ) {
		$classes[] = 'current-menu-item';

	// Everything else that's...
	} elseif (
		// ...not a forum URL,
		( strpos( $current_url, home_url( $forum_slug ) ) === false ) &&
		// ...is a content object or section,
		( is_home() || is_singular( ['page', 'post'] ) || is_archive() ) &&
		// ...and the menu item isn't in the current URL (prevents the "home" link highlighting all).
		strpos( $current_url, $item->url ) !== false
	) {
		$classes[] = 'current-menu-item';
	}

	return $classes;
}, 9, 3 );

/**
 * Site title and logo.
 */
function paperback_title_logo() { ?>
	<div class="site-title-wrap">
		<?php
		// Use the standard Customizer logo.
		$logo = get_theme_mod( 'paperback_customizer_logo' );
		if ( ! empty( $logo ) ) {
			if ( is_front_page() && is_home() ) { ?>
				<h1 class="site-logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" /></a>
				</h1>
			<?php } else { ?>
				<p class="site-logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" /></a>
				</p>
			<?php }
		}
		?>

		<div class="titles-wrap">
			<?php if ( is_front_page() && is_home() ) { ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php } else { ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php } ?>

			<?php if ( get_bloginfo( 'description' ) && is_front_page() && is_home() ) { ?>
				<p class="site-description"><?php bloginfo( 'description' ); ?></p>
			<?php } ?>
		</div>
	</div><!-- .site-title-wrap -->
<?php
}

/**
 * Customizer
 */
require get_stylesheet_directory() . '/inc/customizer.php';

/**
 * Beebees Theme Extras
 */
require get_stylesheet_directory() . '/inc/beebees.php';

/**
 * Forums
 */
if ( class_exists( 'bbPress' ) ) {
	require get_stylesheet_directory() . '/inc/bbpress.php';
}
