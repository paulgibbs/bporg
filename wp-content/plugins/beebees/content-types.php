<?php
namespace beebees\content_types;

/**
 * Content types and taxonomies.
 */

add_action( 'init', function() {
	// Case studies.
	register_extended_post_type( 'case-study', [ 'taxonomies' => [ 'category' ] ], [ 'plural' => 'Case Studies', 'slug' => 'case-study' ] );
	register_extended_taxonomy( 'agency', 'case-study', [ 'meta_box' => 'simple' ], [ 'plural' =>  'Agencies' ] );
} );

add_action( 'cmb2_admin_init', function() {
	$escape_html = function( $value ) {
		return array_map( 'wp_filter_post_kses', $value );
	};

	// Agency term meta.
	$agency = new_cmb2_box( [
		'id'               => 'beebes_agency',
		'new_term_section' => true,
		'object_types'     => [ 'term' ],
		'taxonomies'       => [ 'agency' ],
		'title'            => '',
	 ] );

	$agency->add_field( [
		'id'   => 'beebees_agency_url',
		'name' => 'Website URL',
		'type' => 'text_url',
	 ] );

	$agency->add_field( [
		'id'   => 'beebees_agency_image',
		'name' => 'Logo',
		'type' => 'file',

		'options' => [
			'url' => false,
		],
	 ] );

	// Article meta.
	$agency = new_cmb2_box( [
		'context'      => 'normal',
		'id'           => 'beebes_article',
		'object_types' => get_post_types( [ '_builtin' => true, 'public' => true ], 'names' ),
		'priority'     => 'high',
		'title'        => 'Properties',
	] );

	$agency->add_field( [
		'escape_cb'       => $escape_html,
		'id'              => 'beebees_article_byline',
		'name'            => 'Byline',
		'repeatable'      => true,
		'sanitization_cb' => $escape_html,
		'type'            => 'text',
	 ] );
} );
