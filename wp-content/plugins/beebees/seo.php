<?php
namespace beebees\seo;

use function beebees\settings\{is_buddypress_dot_org, is_bbpress_dot_org};

/**
 * Our expensive SEO consultant's recommendations.
 */

/**
 * Add "noindex,nofollow" robots parameters to old forum topics.
 *
 * Tries to prevent out-of-date forum discussion being visible in search engines.
 */
add_action( 'bbp_head', function() {
	if (
		is_singular() &&
		bbp_is_topic( get_the_ID() ) &&
		( time() - get_post_time( 'U', true ) > YEAR_IN_SECONDS )
	) {
		echo '<meta name="robots" content="noindex,nofollow" />';
	}
} );

/**
 * Add Google Analytics.
 *
 * @link https://dotorg.svn.wordpress.org/buddypress/website/wp-content/plugins/analytics.php
 */
add_action( 'init', function() {
	remove_action( 'wp_footer', 'add_ga' );

	add_action( 'wp_head', function() {
	?>
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-52447-16', 'auto');

		<?php if ( is_buddypress_dot_org() ) : ?>
			ga('create', 'UA-104503819-1', 'auto', 'beebeesTracker');
		<?php elseif ( is_bbpress_dot_org() ) : ?>
			ga('create', 'UA-104503819-2', 'auto', 'beebeesTracker');
		<?php endif; ?>

		ga('send', 'pageview');

		<?php if ( is_buddypress_dot_org() || is_bbpress_dot_org() ) : ?>
			ga('beebeesTracker.send', 'pageview');
		<?php endif; ?>
		</script>
	<?php
	}, 4 );
} );
