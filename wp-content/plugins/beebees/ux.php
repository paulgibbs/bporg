<?php
namespace beebees\ux;

/**
 * User experience tweaks for the Beebees sites (forum, registration, menus, etc).
 */

/**
 * Auto-close topics after 3 months since the last reply.
 *
 * @param bool $is_topic_closed Whether the topic is closed.
 *
 * @return bool True if closed, false if not.
 */
add_filter( 'bbp_is_topic_closed', function( $is_topic_closed, $topic_id ) {
	if ( $is_topic_closed ) {
		return $is_topic_closed;
	}

	$last_active_post_date = get_post_field( 'post_date', bbp_get_topic_last_active_id( $topic_id ) );

	if ( ( time() - strtotime( $last_active_post_date ) ) / MONTH_IN_SECONDS >= 3 ) {
		$is_topic_closed = true;
	}

	return $is_topic_closed;
}, 10, 2 );

/**
 * Only allow 3 published topics from a user in the first 24 hours.
 *
 * If the user has exceeded their limit, move any new topics to moderation queue.
 *
 * @param array $topic_data Topic data.
 *
 * @return array Updated topic data.
 */
add_action( 'bbp_new_topic_pre_insert', function( $topic_data ) {
	$current_user = wp_get_current_user();

	if ( $current_user && time() - strtotime( $current_user->user_registered ) >= DAY_IN_SECONDS ) {
		return $topic_data;
	}

	if ( 'publish' === $topic_data['post_status'] && bbp_get_user_topic_count_raw( $current_user->ID ) >= 3 ) {
		$topic_data['post_status'] = bbp_get_pending_status_id();
	}

	return $topic_data;
} );

/**
 * Remove search from WordPress Toolbar.
 *
 * @param unknown $wp_admin_bar
 */
add_action( 'admin_bar_menu', function( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'search' );
} );
