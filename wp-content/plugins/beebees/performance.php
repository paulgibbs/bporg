<?php
namespace beebees\performance;

use WP_Query;
use function beebees\settings\get_site_type;

/**
 * General .org hosting-specific optimisations.
 */

/**
 * Remove dashboard widgets.
 *
 * The "Right Now" widget uses get_users() and this has caused scaling issues on .org in the past.
 */
add_action( 'wp_dashboard_setup', function() {
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');    // Right Now.
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts.
	remove_meta_box('dashboard_primary', 'dashboard', 'side');        // WordPress blog.
	remove_meta_box('dashboard_secondary', 'dashboard', 'side');      // Other WordPress News.
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
} );

/**
 * Remove Prev/Next header links on un-paginated posts.
 *
 * Reduces amount of DB queries.
 */
add_action( 'the_post', function() {
	if ( is_singular() && isset( $GLOBALS['numpages'] ) && $GLOBALS['numpages'] < 2 ) {
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	}
} );

/**
 * Optimise DB queries from get_posts().
 *
 * @param \WP_Query $wp_query
 */
add_action( 'pre_get_posts', function( WP_Query $wp_query ) {
	// Feeds do not need to know the total count.
	if ( $wp_query->is_feed() ) {
		$wp_query->set( 'no_found_rows', true );
	}
} );

/**
 * Redirect forum search results to /search/.
 *
 * The theme is intended to customise search.php to use Google Custom Search,
 * because we don't have Elasticsearch, and MySQL's plaintext search sucks.
 */
add_action( 'bbp_template_redirect', function() {
	if ( ! bbp_is_search_results() || ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ) {
		return;
	}

	$query = bbp_get_search_terms();
	if ( $query ) {
		$url = sprintf( home_url( '/search/%s/' ), urlencode( $query ) );
		wp_safe_redirect( esc_url_raw( $url ), 301 );
		exit;
	}

	wp_safe_redirect( home_url( '/' ) );
	exit;
} );

/**
 * Optimise forum topic DB queries as much as possible.
 *
 * @param array $args Query arguments.
 *
 * @return array
 */
add_filter( 'bbp_after_has_topics_parse_args', function( $args ) {
	// Feeds do not need to know the total count.
	if ( is_feed() ) {
		$args['no_found_rows'] = true;
	}

	if ( isset( $args['post_type'] ) && $args['post_type'] === bbp_get_topic_post_type() ) {
		// Filter topic queries so they are not sorted by a post meta value.
		if ( isset( $args['meta_key'] ) && ! bbp_is_single_user_topics() ) {

			// bbp_has_topics().
			if ( $args['meta_key'] === '_bbp_last_active_time' ) {
				unset( $args['meta_key'], $args['meta_type'] );
				$args['orderby'] = 'ID';

			// If any other query has a meta_key set, force result ordering by post ID.
			} elseif ( ! empty( $args['meta_key'] ) ) {
				$args['orderby'] = 'ID';
			}
		}
	}

	return $args;
} );

/**
 * Optimise forum reply DB queries as much as possible.
 *
 * @param array $args Query arguments.
 *
 * @return array
 */
add_filter( 'bbp_after_has_replies_parse_args', function( $args ) {
	// Feeds do not need to know the total count.
	if ( is_feed() ) {
		$args['no_found_rows'] = true;
	}

	return $args;
} );

/**
 * Optimise get_lastpostmodified().
 *
 * Forum traffic is high enough that we can avoid a query on post_modified_date
 * and just look at the date on the post with the highest ID. This filters
 * on pre_get_lastpostmodified and caches the result of the simplified query.
 *
 * By using a different cache key, we can avoid constantly modifying this and
 * allow it to time out after five minutes. Otherwise, certain feeds will be
 * always have a changed status.
 *
 * @param string $retval
 * @param string $timezone
 * @param string $post_type
 *
 * @return string
 */
add_filter( 'pre_get_lastpostmodified', function( $retval, $timezone, $post_type ) {
	$timezone    = strtolower( $timezone );
	$cache_group = get_site_type() . '-forums-timeinfo';
	$cache_key   = get_site_type() . ":lastpostmodified:$timezone";

	if ( 'any' !== $post_type ) {
		$cache_key .= ':' . sanitize_key( $post_type );
	}

	$date = wp_cache_get( $cache_key, $cache_group );

	if ( ! $date ) {
		if ( 'any' === $post_type ) {
			$post_types = get_post_types( array( 'public' => true ) );
			$post_types = "'" . implode( "', '", esc_sql( $post_types ) ) . "'";
		} else {
			$post_types = "'" . esc_sql( $post_type ) . "'";
		}

		if ( $timezone === 'gmt' ) {
			$date = $GLOBALS['wpdb']->get_var( "SELECT post_date_gmt FROM {$GLOBALS['wpdb']->posts} WHERE post_status = 'publish' AND post_type IN ({$post_types}) ORDER BY `ID` DESC LIMIT 1" );
		} elseif ( $timezone === 'blog' ) {
			$date = $GLOBALS['wpdb']->get_var( "SELECT post_date FROM {$GLOBALS['wpdb']->posts} WHERE post_status = 'publish' AND post_type IN ({$post_types}) ORDER BY `ID` DESC LIMIT 1" );
		} elseif ( $timezone === 'server' ) {
			$add_seconds_server = date( 'Z' );
			$date = $GLOBALS['wpdb']->get_var( "SELECT DATE_ADD( post_date_gmt, INTERVAL '{$add_seconds_server}' SECOND ) FROM {$GLOBALS['wpdb']->posts} WHERE post_status = 'publish' AND post_type IN ({$post_types}) ORDER BY `ID` DESC LIMIT 1" );
		}

		if ( $date ) {
			wp_cache_set( $cache_key, $date, $cache_group, 5 * MINUTE_IN_SECONDS );
		}
	}

	if ( ! $date ) {
		return $retval;
	} else {
		return $date;
	}
}, 10, 3 );
