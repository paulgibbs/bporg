<?php
namespace beebees\redirects;

use function beebees\settings\{is_buddypress_dot_org, get_qa_forum};

/**
 * Redirects.
 */

/**
 * Perform any general redirects as early as possible.
 */
add_action( 'template_redirect', function() {
	$is_bbpress_active = function_exists( '\bbp_get_root_slug' );
	$forum_root        = home_url( '/' . bbp_get_root_slug() . '/' );

	// Block bbPress user profiles (404).
	if ( $is_bbpress_active && strpos( $GLOBALS['wp']->request, bbp_get_user_slug() ) === 0 ) {
		$GLOBALS['wp_query']->set_404();
		status_header( 404 );
		nocache_headers();
		remove_action( 'template_redirect', 'redirect_canonical' );
		exit;
	}

	// BuddyPress.org.
	if ( is_buddypress_dot_org() && $is_bbpress_active ) {
		// example.com/forums/(.*) -> example.com/support/$1
		do_redirect( [ '/forums/' => $forum_root ], true );

		// example.com/support/topics/.* -> example.com/support/qanda/
		//do_redirect( [ '/support/topics/' => $forum_root . get_qa_forum() . '/' ] );
	}
}, 0 );

/**
 * Keep unprivileged users out of wp-admin.
 */
add_action( 'admin_init', function() {
	if ( ! is_user_logged_in() || ! current_user_can( 'edit_posts' ) ) {
		wp_safe_redirect( home_url( '/' ), 403 );
		exit;
	}
}, 0 );

/**
 * If user is logged in, redirect away from wp-login.php.
 */
add_action( 'init', function() {
	if ( is_user_logged_in() && isset( $GLOBALS['pagenow'] ) && $GLOBALS['pagenow'] === 'wp-login.php' ) {
		wp_safe_redirect( home_url( '/' ) );
		exit;
	}
}, 0 );

/**
 * Do wildcard redirects based on the beginning of the request path.
 *
 * Taken from Automattic VIP's `vip_substr_redirects()`.
 *
 * @param array $redirects      Elements should be in the form of '/some-path/' => 'https://buddypress.org/new/'.
 * @param bool $append_old_path Optional. If true, the full path past the match will be added to the new URL. Defaults to false.
 */
function do_redirect( array $redirects, bool $append_old_path = false ) {
	if ( '/' === $_SERVER['REQUEST_URI'] ) {
		return;
	}

	foreach ( $redirects as $old_path => $new_url ) {
		if ( substr( $_SERVER['REQUEST_URI'], 0, strlen( $old_path ) ) !== $old_path ) {
			continue;
		}

		if ( $append_old_path ) {
			$new_url .= str_replace( $old_path, '', $_SERVER['REQUEST_URI'] );
		}

		wp_safe_redirect( $new_url, 301 );
		exit;
	}
}

/**
 * Disable redirect_guess_404_permalink() for hidden topics.
 *
 * Prevents Spam or Pending topics, that the current user cannot view, from redirecting to unrelated topics.
 *
 * @param string $redirect_to
 *
 * @return string
 */
add_filter( 'bbp_redirect_canonical', function( $redirect_to ) {
	if ( ! is_404() || ! get_query_var( 'name' ) || get_query_var( 'post_type' ) !== bbp_get_topic_post_type() ) {
		return $redirect_to;
	}

	$hidden_topic = get_posts( array(
		'name'                   => get_query_var( 'name' ),
		'no_found_rows'          => true,
		'post_status'            => array( bbp_get_spam_status_id(), bbp_get_pending_status_id() ),
		'post_type'              => bbp_get_topic_post_type(),
		'suppress_filters'       => false,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
	) );
	$hidden_topic = reset( $hidden_topic );

	if ( $hidden_topic && ! current_user_can( 'read_topic', $hidden_topic->ID ) ) {
		$redirect_to = '';
	}

	return $redirect_to;
} );

/**
 * Change bbPress user profile links to instead point to profiles.wordpress.org.
 *
 * @param string $retval
 * @param int    $user_id
 * @param string $user_nicename
 *
 * @return string
 */
add_filter( 'bbp_get_user_profile_url', function( $retval, $user_id, $user_nicename ) {
	return esc_url_raw( "https://profiles.wordpress.org/{$user_nicename}" );
}, 10, 3 );
